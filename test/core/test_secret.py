import unittest
from dataclasses import dataclass
from typing import Tuple, Optional, cast

from sett.core import secret
from sett.core.secret import Secret


class TestSensitive(unittest.TestCase):
    def test_repr(self) -> None:
        scr = Secret("123")
        self.assertEqual(repr(scr), "***")

    def test_format(self) -> None:
        scr = Secret("123")
        self.assertEqual(f"{scr}", "***")

    def test_enforce_secret_by_signature_args(self) -> None:
        def f(scr: Secret[str], x: int) -> Tuple[Secret[str], int]:
            return scr, x

        args, kwargs = secret.enforce_secret_by_signature(f, ("123", 1), {})
        self.assertEqual([type(a) for a in args], [Secret, int])
        self.assertEqual(kwargs, {})

    def test_enforce_secret_by_signature_kwargs(self) -> None:
        def f(scr: Secret[str], x: int) -> Tuple[Secret[str], int]:
            return scr, x

        args, kwargs = secret.enforce_secret_by_signature(f, (), {"scr": "123", "x": 1})
        self.assertEqual(args, ())
        self.assertEqual(
            {key: type(val) for key, val in kwargs.items()}, {"scr": Secret, "x": int}
        )

    def test_enforce_secret_by_signature(self) -> None:
        def f(
            scr: Secret[str], x: int, *, scr2: Secret[str], y: bool
        ) -> Tuple[Secret[str], int, Secret[str], int]:
            return scr, x, scr2, y

        args, kwargs = secret.enforce_secret_by_signature(
            f, ("123", 1), {"scr2": "123", "y": False}
        )
        self.assertEqual([type(a) for a in args], [Secret, int])
        self.assertEqual(
            {key: type(val) for key, val in kwargs.items()}, {"scr2": Secret, "y": bool}
        )

    def test_dataclass(self) -> None:
        # Verify that, when a class has a "Secret" type attribute or has
        # a nested "Secret" type attribute, the "enforce_secret()" function
        # will correctly hide the content of the secret.
        # Note that in the nested class the type of the secret attribute "scr"
        # Optional, while it is not in the outer class. This is to test both
        # types: optional secret and regular secret.
        @dataclass
        class DummyInteriorClassWithSecret:
            scr: Optional[Secret[str]]
            x: float

        @dataclass
        class DummyClassWithSecret:
            scr: Secret[str]
            s: str
            interior_scr: DummyInteriorClassWithSecret

        # Create new instances of the classes with secrets. Note that we set
        # the values of "scr" (secrets) to plain strings. They will later be
        # hidden.
        interior_secret_content = "hide me inside!"
        secret_content = "hide me too!"
        interior_object_with_secret = DummyInteriorClassWithSecret(
            scr=cast(Optional[Secret[str]], interior_secret_content), x=1.0
        )
        object_with_secret = DummyClassWithSecret(
            scr=cast(Secret[str], secret_content),
            s="this is not a secret",
            interior_scr=interior_object_with_secret,
        )

        # When the secrets are not "enforced", the content of the secrets
        # should be accessible (visible in plain text).
        self.assertTrue(interior_secret_content in repr(interior_object_with_secret))
        self.assertTrue(secret_content in repr(object_with_secret))

        # When "enforce_secret()" is called, the content of the secrets should
        # now be hidden.
        object_with_hidden_secret = secret.enforce_secret(
            object_with_secret, DummyClassWithSecret
        )
        self.assertTrue(secret_content not in repr(object_with_hidden_secret))
        self.assertTrue(interior_secret_content not in repr(object_with_hidden_secret))

        # The non-secret attributes should still be visible (not hidden).
        self.assertTrue("this is not a secret" in repr(object_with_hidden_secret))
        self.assertTrue("x=1.0" in repr(object_with_hidden_secret))

    def test_optional(self) -> None:
        type_to_test = cast(type, Optional[Secret])
        self.assertTrue(
            "xxx" not in repr(secret.enforce_secret(val="xxx", t=type_to_test))
        )
