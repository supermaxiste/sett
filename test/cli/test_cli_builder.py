import unittest
from unittest.mock import patch
from typing import (
    Any,
    Callable,
    Iterable,
    List,
    Optional,
    Sequence,
    Tuple,
    TypeVar,
    cast,
)
import argparse
from sett.cli import cli_builder


F = TypeVar("F", bound=Callable[..., None])


def cli_args(args: Sequence[str]) -> Callable[[F], F]:
    def dec(fn: F) -> F:
        setattr(fn, "test_args", args)
        return fn

    return dec


@cli_args(["-x", "1", "--longname"])
def f(x: int, longname: bool) -> None:
    assert x == 1 and longname


@cli_args(["1"])
def g(x: int, *, longname: bool) -> None:
    assert x == 1 and not longname


@cli_args(["--longname", "1", "2"])
def h(x: List[int], *, longname: bool = False) -> None:
    assert x == [1, 2] and longname


@cli_args([])
def i(x: Sequence[int] = (), *, _longname: bool = False) -> None:
    assert x == ()


@cli_args(["--lst", "1", "--lst", "2"])
def f_seq(*, lst: List[int]) -> None:
    assert lst == [1, 2]


class TestAddArgumentsBySignature(unittest.TestCase):
    def setUp(self) -> None:
        self.functions: Sequence[Callable[..., None]] = (f, g, h, i, f_seq)

    def test_arguments_by_signature(self) -> None:
        for fn in self.functions:
            with self.subTest(f=fn.__name__):
                parser = argparse.ArgumentParser()
                for arg in cli_builder.arguments_by_signature(fn):
                    arg.add_to_parser(cast(cli_builder.ParserLike, parser))
                cmd_args = parser.parse_args(getattr(fn, "test_args"))
                fn(**vars(cmd_args))

    def test_required_positional_nargs(self) -> None:
        x_ref = (3, 5)

        def f_no_default(x: Sequence[int], *, _longname: bool = False) -> None:
            self.assertSequenceEqual(x, x_ref)

        parser = argparse.ArgumentParser()
        args = cli_builder.arguments_by_signature(f_no_default)
        for arg in args:
            arg.add_to_parser(cast(cli_builder.ParserLike, parser))
        with self.subTest("With positional arguments"):
            cmd_args = parser.parse_args([str(ref) for ref in x_ref])
            f_no_default(**vars(cmd_args))
        with self.subTest("Positional arguments missing"):
            with patch(
                "sett.cli.cli_builder.argparse.ArgumentParser.error"
            ) as mock_error:
                parser.parse_args([])
                mock_error.assert_called_once_with(
                    "the following arguments are required: x"
                )

    def test_arguments_by_signature_optional(self) -> None:
        def opt_f(s: Optional[str]) -> None:
            assert s == "s"

        def to_argument_list(l: Iterable[Any]) -> Sequence[cli_builder.Argument]:
            return cast(Sequence[cli_builder.Argument], list(l))

        with self.assertRaises(ValueError):
            _ = to_argument_list(cli_builder.arguments_by_signature(opt_f))

        args = to_argument_list(
            cli_builder.arguments_by_signature(opt_f, overrides={"s": {"default": "s"}})
        )
        parser = argparse.ArgumentParser()
        for arg in args:
            parser.add_argument(*arg.args, **arg.kwargs)
            cmd_args = parser.parse_args([])
        opt_f(**vars(cmd_args))
        cmd_args = parser.parse_args(["-s", "s"])
        opt_f(**vars(cmd_args))

    def test_invalid_override(self) -> None:
        with self.assertRaises(ValueError):
            list(cli_builder.arguments_by_signature(f, overrides={"y": {}}))


class TestSubcommands(unittest.TestCase):
    def setUp(self) -> None:
        self.functions: List[Callable[..., Any]] = [f, g, h]

    def test_subcommands(self) -> None:
        class Cli(cli_builder.CliWithSubcommands):
            subcommands = tuple(cli_builder.Subcommand(fn) for fn in self.functions)

        for fn in self.functions:
            with self.subTest(f=fn.__name__):
                Cli([fn.__name__] + getattr(fn, "test_args"))


F2 = TypeVar("F2", bound=Callable[..., Any])


class TestUtils(unittest.TestCase):
    def _decorator_routine_check(self, fct: F2, decorator: Callable[[F2], F2]) -> None:
        self.assertEqual(decorator(fct).__name__, fct.__name__)
        self.assertEqual(decorator(fct).__doc__, fct.__doc__)

    def test_lazy_partial(self) -> None:
        def _f(x: int, y: int, z: int = -1, w: int = -2) -> Tuple[int, int, int, int]:
            return x, y, z, w

        self._decorator_routine_check(_f, cli_builder.lazy_partial())
        _g = cli_builder.lazy_partial(lambda: 1, w=lambda: 4)(_f)
        self.assertEqual(_g(2, 3), (1, 2, 3, 4))
        self.assertEqual(_g(y=2, z=3, w=5), (1, 2, 3, 5))

    def test_lazy_partial_preserves_keywords(self) -> None:
        def _f(x: int, y: int, z: int = -1, w: int = -2) -> Tuple[int, int, int, int]:
            return x, y, z, w

        setattr(_f, "keywords", {"z": -1})

        def cb() -> int:
            return 1

        _g = cli_builder.lazy_partial(cb)(_f)
        self.assertEqual(getattr(_g, "keywords"), getattr(_f, "keywords"))
        self.assertEqual(getattr(_g, "args"), (cb,))

    def test_partial(self) -> None:
        def _f(x: int, y: int, z: int = -1, w: int = -2) -> Tuple[int, int, int, int]:
            return x, y, z, w

        self._decorator_routine_check(_f, cli_builder.partial())
        _g = cli_builder.partial(1, w=4)(_f)
        self.assertEqual(_g(2, 3), (1, 2, 3, 4))
        self.assertEqual(_g(y=2, z=3, w=5), (1, 2, 3, 5))

    def test_partial_f_with_keywords_field(self) -> None:
        def _f(x: int, y: int, z: int = -1, w: int = -2) -> Tuple[int, int, int, int]:
            return x, y, z, w

        setattr(_f, "keywords", {"z": 42})
        self._decorator_routine_check(_f, cli_builder.partial())
        _g = cli_builder.partial(1, w=4)(_f)
        self.assertEqual(_g(2, z=10), (1, 2, 10, 4))
        self.assertEqual(_g(y=2, w=5), (1, 2, 42, 5))

    def test_rename(self) -> None:
        def _f() -> int:
            return 1

        _g = cli_builder.rename("g")(_f)
        self.assertEqual(_g.__name__, "g")
        self.assertEqual(_f.__doc__, _g.__doc__)
        self.assertEqual(_f(), _g())

    def test_set_default(self) -> None:
        def _f(x: int, y: int, z: int = -1, w: int = -2) -> Tuple[int, int, int, int]:
            return x, y, z, w

        self._decorator_routine_check(_f, cli_builder.set_default())
        _g = cli_builder.set_default(w=4)(_f)
        self.assertEqual(_g(2, 3), (2, 3, -1, 4))
        self.assertEqual(_g(x=1, y=2, z=3, w=5), (1, 2, 3, 5))
