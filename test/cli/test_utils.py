import unittest

from sett.cli import get_passphrase_from_cmd
from sett.core.error import UserError


class TestPassphraseFromCmd(unittest.TestCase):
    def test_success(self) -> None:
        for text in ("very secret", "  verysecret \n"):
            self.assertEqual(
                get_passphrase_from_cmd(f"echo {text}").reveal(), text.strip()
            )

    def test_fail(self) -> None:
        with self.assertRaises(UserError) as cm:
            get_passphrase_from_cmd("ls file_is_missing")
        self.assertIn("Failed to read passphrase from", format(cm.exception))
        self.assertNotIn("b'", format(cm.exception))
