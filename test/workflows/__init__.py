import json
import copy
import logging
import unittest
import contextlib
from types import TracebackType
from typing import Optional, Type, Any, Dict
from unittest import mock

from libbiomedit.lib.secret import Secret
from libbiomedit.metadata import DATE_FMT

from sett.core import gpg
from sett.core.error import UserError
from sett.core.metadata import Purpose, MetaData, HexStr1024, HexStr256


class DisableLogging:
    def __enter__(self) -> None:
        logging.disable(logging.CRITICAL)

    def __exit__(
        self,
        exc_type: Optional[Type[BaseException]],
        exc_value: Optional[BaseException],
        exc_traceback: Optional[TracebackType],
    ) -> None:
        logging.disable(logging.NOTSET)


class TestCaseWithRaiseError(unittest.TestCase):
    """unittest.TestCase class with a custom context manager."""

    def assert_error_raised_if_needed(
        self, should_raise_error: bool, error_type: type = UserError
    ) -> Any:
        """Optional context manager that checks that a UserError is raised"""
        return (
            self.assertRaises(error_type)
            if should_raise_error
            else contextlib.nullcontext()
        )


class MockProgressbar(mock.Mock):
    """A fake progress bar to be used for testing."""

    def __init__(self, *args: Any, **kwargs: Any):
        super().__init__(*args, **kwargs)
        self.value = 0

    def update(self, x: int) -> None:
        self.value = x

    def get_completed_fraction(self) -> int:
        return self.value


class MockPortalAPI(mock.Mock):
    """A fake portal API for testing purposes."""

    def __init__(self, *args: Any, **kwargs: Any):
        super().__init__(*args, **kwargs)
        self.project_code = TEST_PROJECT_CODE

    def verify_dpkg_metadata(  # pylint: disable=unused-argument
        self,
        metadata: MetaData,
        file_name: str,
    ) -> str:
        # Define authorized data senders and recipients.
        cn_as_hex = HexStr1024(CHUCK_NORRIS_KEY.fingerprint)
        sh_as_hex = HexStr1024(SGT_HARTMANN_KEY.fingerprint)
        authorized_senders = (cn_as_hex,)
        authorized_recipients = (cn_as_hex, sh_as_hex)

        # Raise error if something is not right.
        if metadata.transfer_id != 23:
            raise RuntimeError(f"DTR ID {metadata.transfer_id} not found...")
        if metadata.purpose is Purpose.PRODUCTION:
            raise RuntimeError("Wrong purpose...")
        if metadata.sender not in authorized_senders:
            raise RuntimeError("Sender key is not authorized...")
        if any(x not in authorized_recipients for x in metadata.recipients):
            raise RuntimeError("Recipient key is not a DM for the project...")

        # If all project metadata is correct, return the project's code.
        return self.project_code


def mock_key(full_name: str, email: str, fingerprint: str) -> gpg.Key:
    """Generate a mock PGP key."""

    uid = gpg.Uid(full_name=full_name, email=email)
    self_signature = gpg.Signature(
        issuer_uid=uid,
        issuer_key_id=fingerprint[-16:],
        issuer_fingerprint=fingerprint,
        creation_date="1550241679",
        signature_class="13x",
        validity=gpg.model.SignatureValidity.good,
    )
    return gpg.Key(
        key_id=fingerprint[-16:],
        fingerprint=fingerprint,
        validity=gpg.Validity.ultimately_valid,
        key_length=4096,
        pub_key_algorithm=1,
        creation_date="1550241679",
        uids=(uid,),
        owner_trust="u",
        key_type=gpg.KeyType.public,
        origin="http://hagrid.hogwarts.org:11371",
        signatures=(self_signature,),
        key_capabilities=frozenset(
            (
                gpg.KeyCapability.sign,
                gpg.KeyCapability.certify,
                gpg.KeyCapability.encrypt,
            )
        ),
    )


CHUCK_NORRIS_KEY = mock_key(
    full_name="Chuck Norris",
    email="chuck.norris@roundhouse.gov",
    fingerprint="AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
)
SGT_HARTMANN_KEY = mock_key(
    full_name="Gunnery Sgt. Hartmann",
    email="hartmann@bullshit.gov",
    fingerprint="DA1A363BC5E11DA73806A8A4E3E4024A0E56221E",
)

FAKE_CHUCK_NORRIS_KEY = mock_key(
    full_name="Chuck Lorris",
    email="chuck.norris@roundhouse.com",
    fingerprint="AAAAAAAAAAAAAAAAAAAAAAAAAAAAAABBBBBBBBBB",
)

CHUCK_NORRIS_PWD = Secret("Chuck Norris needs no password, he *is* the password!")
TEST_PROJECT_CODE = "ROUNDHOUSEKICK"

TEST_METADATA_DICT: Dict[str, Any] = {
    "sender": CHUCK_NORRIS_KEY.fingerprint,
    "recipients": [CHUCK_NORRIS_KEY.fingerprint, SGT_HARTMANN_KEY.fingerprint],
    "checksum": "abcdef0123456789" * 4,
    "timestamp": "2022-02-02T11:33:55+0100",
    "version": MetaData.version,
    "checksum_algorithm": "SHA256",
    "compression_algorithm": "gzip",
    "transfer_id": 23,
    "purpose": Purpose.TEST.value,
}


def mock_metadata_generator(**kwargs: Any) -> MetaData:
    metadata_copy = copy.deepcopy(TEST_METADATA_DICT)
    metadata_copy.update(kwargs)
    return MetaData.from_dict(metadata_copy)


def encode_metadata_as_file(metadata: MetaData) -> bytes:
    return json.dumps(MetaData.asdict(metadata), indent=4).encode()
