import os
from unittest import mock

import pytest

from sett.workflows.config import create
from . import DisableLogging


@pytest.mark.parametrize("file_exists", (True, False))
def test_create(file_exists: bool) -> None:
    """Test create config file.

    There are 2 cases to test: when a config files already exists and when not.
    """
    mock_create_config = mock.Mock()
    # Replace functions called in the workflow by mock objects.
    with mock.patch(
        "sett.workflows.config.create_config", mock_create_config
    ), mock.patch.object(os.path, "isfile", lambda _: file_exists), DisableLogging():
        # Run function to test
        create()

        # Verify the function to create a new config file was only
        # called when needed.
        if file_exists:
            mock_create_config.assert_not_called()
        else:
            mock_create_config.assert_called_once()
