sett - secure encryption and transfer tool
==========================================

|version| |license| |source| |python|

Welcome to the official **sett** documentation page. Please use the table of
contents menu to the left or below to navigate the topics.


.. toctree::
  :maxdepth: 3
  :caption: Table of Contents

  overview.rst
  quick_start.rst
  installation.rst
  key_management.rst
  generating_ssh_keys.rst
  usage.rst
  automatization.rst
  config_options.rst
  packaging_specifications.rst
  benchmarks.rst
  faq.rst
  source_location.rst


.. |version| image:: https://img.shields.io/pypi/v/sett.svg?style=flat-square&label=latest%20version
    :target: https://pypi.org/project/sett
    :alt: Latest stable released

.. |license| image:: https://img.shields.io/badge/License-GPLv3-blue.svg?style=flat-square&color=green&label=license
    :target: https://www.gnu.org/licenses/gpl-3.0
    :alt: License

.. |source| image:: https://img.shields.io/badge/source_code-GitLab-orange?style=flat-square&logo=Git&logoColor=white
    :target: https://gitlab.com/biomedit/sett
    :alt: Source code on GitLab

.. |python| image:: https://img.shields.io/pypi/pyversions/sett.svg?style=flat-square&logo=python&logoColor=white&label=supported%20python%20versions&color=purple
    :target: https://pypi.org/project/sett
    :alt: Supported python versions
